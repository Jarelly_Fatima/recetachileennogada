<html>
<head>
<style>
h1 {
        text-align: center;
}
h2 {
        text-align: center;
}
body {
        background-color: DFFC8B;
        margin: 0 20%;
}
</style>
</head>
<body>

<h1 > <FONT FACE="impact" SIZE=6 COLOR="brown"> Chile en nogada</FONT> </h1>
<h2 aling="left"> <i>Historia</i> </h2>
<p> Los chiles en nogada tienen su leyenda propia, pues se dice que cuando Agustín de Iturbide pasó,junto con el Ejército Trigarante, por Puebla rumbo a la Ciudad de México en septiembre de 1821 </p>
<p>tras haber firmado los Tratados de Córdoba, las monjas agustinas del convento de Santa Mónica decidieron hacerlo el 28 de agosto con motivo de su santo con un platillo original. Tomando </p>
<p>como referencia el símbolo del Ejército Trigarante, que era una bandera de colores blanco,verde y rojo, los cuales representaban las tres garantías; religión, unión e independencia.</p>
<h2> <i>Receta</i> </h2>
<p><b> INGREDIENTES: </b></p>
<ul>
<li>6 chiles poblanos grandes </li>
<li>Hierbas de olor </li>
<li>300 gramos de carne molida de res </li>
<li> 2 jitomates asados sin piel </li>
<li>1/2 cebolla </li>
<li>2 dientes de ajo </li>
<li>1 plátano macho frito (picado en cuadritos) </li>
<li>1/2 taza de almendras peladas  </li>
<li> 1 manzana pelada y picada </li>
<li>1 durazno pelado y picado </li>
<li>1/2 taza de pasitas </li>
<li> 100 gramos de acirtrón picado</li>
<li>50 gramos de piñones</li>
<li>100 gramos de nueces en trocitos</li>
<li>Azúcar  </li>
<li>Aceite  </li>
<li>Sal y pimienta </li>
</ul>
<p><b>Ingredientes para la nogada:</b></p>
<ul>
<li>200 gramos de nuez de castilla </li>
<li>1/2 litro de crema ácida</li>
<li>190 gramos de queso crema </li>
<li>Un chorrito de vino blanco </li>
<li>2 cucharadas de queso de cabra </li>
<li>1/2 taza de leche evaporada </li>
<li>1/2 cucharadita de canela en polvo</li>
<li>Azúcar</li>
<li>Sal</li>
</ul>
<p><b>Ingredientes para el emplatado:</b></p>
<ul>
<li>Granos de granada </li>
<li>Hojas de perejil </li>
<h2>PREPARACION</h2>
<ol>
<li>Asa los chiles y mételos en una bolsa de plástico para que suden, después pélalos y desvénalos. Cuida que no se rompan. </li>
<li>Muele los jitomates y fríelos con la cebolla, almendras, ajo, acitrón, pasas, piñones, nueces y frutas en cuadritos. </li>
<li>Agrega la carne molida, azúcar, sal y pimienta. Cocina a fuego medio. </li>
</ol>
<p><b>Para la nogada</b></p>
<ol>
<li> Licúa todos los ingredientes. </li>
<li>Puedes modificar las porciones para logar la textura y sabor que más te guste. </li>
</ol>
<p><b>Para el emplatado:</b></p>
<ol>
<li>Rellena los chiles. </li>
<li>Acomoda uno en cada plato y báñalos con la nogada. </li>
<li>Decora con granos de granada y hojitas de perejil. </li>
</ol>
<h1>¡¡LISTO A DISFRUTAR!!</h1>
</body>
</html>